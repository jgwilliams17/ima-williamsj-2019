function [] = extract_ss_flow_data()

    % pressure is in units of cmH2O - convert to barye (cgs unit of
    % pressure) by multiplying by rho*g
    num_kidneys = 3; % experiments done on three different kidneys
    rho = 1;
    g = 981; 
    
    for i = 1:num_kidneys

%% define the number of data sets per kidney & intervals were inlet pressure const (end of interval to allow for flow & pressure to reach steady-state):

        if i == 1
            num_data_sets = 5; % each kidney has different number of data sets
            min_t_1 = [75 107 123 138 153 180 197 209 220 245]; % start of five sec. intervals inlet p approx. const.
            min_t_2 = [85 135 162 182 203 238 260 289 303 317 341 360];
            min_t_3 = [70 105 127 147 169 204 225 241 255 271 285 305 323 345];
            min_t_4 = [50 80 93 119 153 180 201 223 240 253 279 297 321];
            min_t_5 = [40 130];
        elseif i == 2
            num_data_sets = 2;
            min_t_1 = [50 70 89 109 120 139 151 165 187 201 211 227 242 252 267 281 299 316];
            min_t_2 = [90 118 137 159 187 215 235 255 281 304 331 353 377];
        elseif i == 3
            num_data_sets = 4;
            min_t_1 = [95 122 150];
            min_t_2 = [75 121];
            min_t_3 = [55 99];
            min_t_4 = [60 91 110 125 141 163 190 225];
        end

%% for each data-set, take mean & std. of flow rate in interval:

        for k = 1:num_data_sets
            dataset=sprintf('raw-data/K%.0f_%.0f',i,k); % load the raw data
            timeset=sprintf('min_t_%.0f',k);
            load(dataset);
            min_t=eval(timeset); % starts of all the 5 sec. increments
            num_inlet_pressure = length(min_t); % number of increments
            mean_inlet_pressure = zeros(num_inlet_pressure,1);
            std_inlet_pressure = zeros(num_inlet_pressure,1);
            systematic_flow_rate_error = zeros(num_inlet_pressure,1);
            mean_flow_rate = zeros(num_inlet_pressure,1);
            std_flow_rate = zeros(num_inlet_pressure,1);

%% go through each of 5 sec. intervals: 

            for j = 1:length(min_t)
                indx_pressure = find(time_pressure_data > min_t(j) & time_pressure_data < min_t(j)+5);
                indx_mass = find(time_mass_data > min_t(j) & time_mass_data < min_t(j)+5);
                mean_inlet_pressure(j) = mean(inlet_pressure(indx_pressure))*rho*g;
                mean_kidney_pressure(j) = mean(kidney_pressure(indx_pressure))*rho*g;
                std_kidney_pressure(j) = std(kidney_pressure(indx_pressure))*rho*g;
                mean_kidney_mass(j) = mean(kidney_mass(indx_mass))-kidney_mass(1);
                std_kidney_mass(j) = std(kidney_mass(indx_mass));
                std_inlet_pressure(j) = std(inlet_pressure(indx_pressure)*rho*g);
                mean_flow_rate(j) = mean((diff(out_mass(indx_mass)))./diff(time_mass_data(indx_mass)));
                std_flow_rate(j) = std((diff(out_mass(indx_mass)))./diff(time_mass_data(indx_mass)));
                systematic_flow_rate_error(j) = (1/(length(indx_mass)-1))*sqrt(2)*0.005*sqrt(sum(1./(diff(time_mass_data(indx_mass)).^2)));
            end
            systematic_pressure_meas_error = 0.005*600*1333.223900000007;
            zero_pressure = kidney_pressure(1)*rho*g;
            filename=sprintf('ss-data/K%.0f_%.0f_ss',i,k);
            save(filename,'mean_kidney_mass','mean_kidney_pressure','std_kidney_mass','std_kidney_pressure','mean_flow_rate','std_flow_rate','mean_inlet_pressure','std_inlet_pressure','zero_pressure','systematic_pressure_meas_error','systematic_flow_rate_error');
        end
    end
    
end