clear all;
close all;
set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex'); set(0,'defaulttextInterpreter','latex');
colors = get(gca,'colororder'); 
addpath('useful_functions/');

%% set these if want to re-write data to csv files for plots:

write_raw_data_to_csv = true;
write_mean_data_to_csv = true;
write_fit_to_csv = true;

incr = 3; % save every incr raw data point

% convert from cgs units to cm h2o:
barye2cmh2o=0.00101972;

% pressure measurement error (FISO Fibre Optic Pressure Sensor):
systematic_pressure_meas_error = 0.005*600*1333.223900000007;
all_pressure = [];
all_pressure_error = []; % systematic pressure error is different for each one b/c involves relative pressure calculation

mass_incr = 0.1; % interpolate all values at this increment
mass_interp = [0:mass_incr:7]; % go up to 7 grams

%% Plot gradual
% use the gradual increase data sets to estimate kidney compliance

kidneys_w_gradual_increase = [3];
for i = 1:length(kidneys_w_gradual_increase)
    num_kidney = kidneys_w_gradual_increase(i);

    if num_kidney == 1
        data_set = [5];
        dataset=sprintf('trimmed-data/K%.0f_%.0f_Trim',num_kidney,1);
        load(dataset);
        % kidney maintains some fluid after each run - set the "zero mass"
        % to be in the mass in the first gradual run from each kidney 
        zero_mass = kidney_mass(1);
    elseif num_kidney == 3
        data_set = [2 3];
        dataset=sprintf('trimmed-data/K%.0f_%.0f_Trim',num_kidney,2);
        load(dataset);
        zero_mass = kidney_mass(1);
    end
    
    for j = 1:length(data_set)
    
        %% Make time-step uniform so can plot volume/pressure

        dataset=sprintf('trimmed-data/K%.0f_%.0f_Trim',num_kidney,data_set(j));
        load(dataset);
        % zero_mass = kidney_mass(1);
        
        %% Fit compliance
        kidney_mass = kidney_mass-zero_mass;
        kidney_pressure = kidney_pressure*barye2cmh2o;
        kidney_pressure_error = systematic_pressure_meas_error*ones(size(kidney_pressure))'*barye2cmh2o;
        plt(:,i)=errorbarxy(kidney_mass,kidney_pressure,0.005*ones(size(time)),kidney_pressure_error,'Color',colors(i,:),'Linestyle','none','Marker','o','MarkerFaceColor',colors(i,:),'Linewidth',1,'MarkerSize',4);
        hold on
        
        %% Make mass increment uniform accross data sets 
        
        [m_unique,index] = unique(kidney_mass);
        kidney_press_interp = interp1(m_unique,kidney_pressure(index),mass_interp(mass_interp<max(kidney_mass)));
        kidney_press_interp = [kidney_press_interp'; nan(length(mass_interp)-length(kidney_press_interp),1)];
        
        all_pressure=[all_pressure kidney_press_interp];
        all_pressure_error = [kidney_pressure_error all_pressure_error];
        
        if write_raw_data_to_csv
            % write data to csv:
            headers = {'kmass','kp','kmass_sd','kp_sd'};
            data = [kidney_mass(1:incr:end) kidney_pressure(1:incr:end) 0.005*ones(size(kidney_pressure(1:incr:end))) [kidney_pressure_error(1:incr:end)]'];
            filename = sprintf('csv-files/compliance-k%i-%i.csv',num_kidney,data_set(j));
            csvwrite_with_headers(filename,data,headers)
        end
    end
end

% plot the mean interpolated value and combined standard deviations: 
plot(mass_interp,nanmean(all_pressure,2),'k');
hold on
errorbar(mass_interp,nanmean(all_pressure,2),sqrt(2)*(nanstd(all_pressure,0,2)+mean(all_pressure_error)),'k');

% write the mean data to a csv:
if write_mean_data_to_csv
    headers = {'kmass','kp','kmass_sd','kp_sd'};
    data = [mass_interp' nanmean(all_pressure,2) sqrt(2)*0.005*ones(size(mass_interp))' sqrt(2)*(nanstd(all_pressure,0,2)+mean(all_pressure_error)*ones(size(nanstd(all_pressure,0,2))))];
    csvwrite_with_headers('csv-files/compliance-data.csv',data,headers)
end

%% Fit an exponential model to all the data:

% fit exponential model to data:
volfun_exp = @(b,m) b(1).*(exp(b(2).*m)-1);
bfit_exp = nlinfit(mass_interp',nanmean(all_pressure,2),volfun_exp,[1, 0.69]);
expon=plot(mass_interp,volfun_exp(bfit_exp,mass_interp),'r--','Linewidth',3);
save('expfit.mat','bfit_exp');

% linear model (oratis or a linear fit):
volfun_lin = @(m,b) b*m;
bfit_lin = nlinfit(mass_interp',nanmean(all_pressure,2),volfun_lin,0.413);
lin=plot(mass_interp,volfun_lin(mass_interp,bfit_lin),'k--','Linewidth',3);
oratis=plot(mass_interp,volfun_lin(mass_interp,0.41),'b--','Linewidth',3);

%% Make plot:

legend([plt(1,1) expon lin oratis],{'Data','Exponential fit','Linear fit','Oratis'});
set(legend,'fontsize',14,'location','northwest');
grid on
% xlim([0 7]);
xlabel('Mass increase (g)','fontsize',18);
ylabel('Relative pressure increase (dim.less)','fontsize',18);

%% write fit to csv:

if write_fit_to_csv
    headers = {'mass','pressure_oratis','pressure_lin','pressure_exp'};
    data = [mass_interp' volfun_lin(mass_interp,0.41)' volfun_lin(mass_interp,bfit_lin)' volfun_exp(bfit_exp,mass_interp)'];
    csvwrite_with_headers('csv-files/compliance-fit.csv',data,headers)
end