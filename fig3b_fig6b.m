clear all;
close all;

addpath('useful_functions/'); % add offset_circles_analytical.m

write_model_compare_to_csv = true;
write_td_pressure_to_csv = false;

%% Dimensional parameters:

rho = 1;
g = 981;
Pin = rho*g*60; % hydrostatic pressure of 60 cm 
mu = 0.01;

r_wc = 0.06; % working-channel radius (cm)
r_sc = 0.31667/2; % outer radius of scope
r_sh = 0.152*2.54/2; % inner radius of sheath

barye2cmh2o=0.00101972;

La = 79;
Lb = 36;

%% Resistances (including "optimal" one):

Rin = 8/(pi*r_wc^4);
Rout_offset = -1/offset_circles_analytical(r_sc,r_sh,0.99,50);
Rout_centre = 1/(pi/8*(r_sh^4-r_sc^4-(r_sh^2-r_sc^2)^2/(log(r_sh/r_sc)))); % lower bound on flow rate
Rout_optimal = Rout_centre/5.635;

%% Analytical solutions:

load('expfit.mat');
C = bfit_exp(2);
Pstar = bfit_exp(1)/barye2cmh2o;
tau = @(rin,rout) 1+Pin/Pstar+(rin*La)/(rout*Lb);
pk_nonlin = @(t,rin,rout) Pin*(exp(tau(rin,rout)*(t/(La*rin*mu/(C*Pstar))))-1)./((Pin/Pstar)+exp(tau(rin,rout)*t/(La*rin*mu/(C*Pstar)))*(1+(rin*La)/(rout*Lb)));

K_oratis = 0.41; % cmh2o/ml
K_fit = 9.5906;
K_fit = K_fit/barye2cmh2o;
K_oratis = K_oratis/barye2cmh2o;
tau_oratis = @(rin,rout) (La*rin*mu/K_oratis)*(1+(La*rin/(Lb*rout)))^(-1);
tau_fit = @(rin,rout) (La*rin*mu/K_fit)*(1+(La*rin/(Lb*rout)))^(-1);
pk_oratis = @(t,rin,rout) Pin*(1+rin*La/(rout*Lb))^(-1)*(1-exp(-t/tau_oratis(rin,rout)));
pk_lin_fit = @(t,rin,rout) Pin*(1+rin*La/(rout*Lb))^(-1)*(1-exp(-t/tau_fit(rin,rout)));

%% Compare models (effect of compliance):

time_exp = linspace(0,60,100);
time_lin = linspace(0,10*60,100);

figure(1)
plot(time_exp,pk_nonlin(time_exp,Rin,Rout_centre)/(rho*g),'Linewidth',2)
hold on
plot(time_lin,pk_oratis(time_lin,Rin,Rout_centre)/(rho*g),'Linewidth',2)
plot(time_exp,pk_lin_fit(time_exp,Rin,Rout_centre)/(rho*g),'Linewidth',2)
legend('exponential','oratis','linear fit')
xlim([0 60])

if write_model_compare_to_csv
    headers = {'time_exp','time_lin','exp','lin','lin_fit'};
    data = [time_exp' time_lin' [pk_nonlin(time_exp,Rin,Rout_centre)/(rho*g)]' [pk_oratis(time_lin,Rin,Rout_centre)/(rho*g)]' [pk_lin_fit(time_exp,Rin,Rout_centre)/(rho*g)]'];
    csvwrite_with_headers('csv-files/compare-models.csv',data,headers);
end
%% Time dependent pressure (effect of resistance):

figure(3)
minp = pk_nonlin(time_exp,Rin,Rout_offset)/(rho*g);
maxp = pk_nonlin(time_exp,Rin,Rout_centre)/(rho*g);
optimalp = pk_nonlin(time_exp,Rin,Rout_optimal)/(rho*g);

plot(time_exp,minp,'Linewidth',2)
hold on
plot(time_exp,maxp,'Linewidth',2)
plot(time_exp,optimalp,'Linewidth',2)

if write_td_pressure_to_csv
    headers = {'time','minp','maxp','optimal'};
    data = [time_exp' minp' maxp' optimalp'];
    csvwrite_with_headers('csv-files/sheath-resistance.csv',data,headers)
end