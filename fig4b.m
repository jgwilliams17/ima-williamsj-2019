clear all;
close all;
set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex'); set(0,'defaulttextInterpreter','latex');
colors = get(gca,'colororder'); % used to specify diff color for each kidney 

addpath('useful_functions/'); % add csvwrite & errorbarxy

%% parameters

rho = 1; % density (cgs)
g = 981; % gravity (cgs)

r_wc = 0.06; % working-channel radius (cm)
r_sc = 0.31667/2; % outer radius of scope

% From specifications (access sheath diagram Niraj sent through):

d_sh_in = 0.152; % inches
d_sh_tol_in = 0.002; % inches
r_sh = d_sh_in*2.54/2;
r_sh_max = (d_sh_in+d_sh_tol_in)*2.54/2;
r_sh_min = (d_sh_in-d_sh_tol_in)*2.54/2;

L_scope = 79; % length of scope
L_sheath = 36; % length of sheath

phi = 0.99; % position corresponding to offset scope in sheath
mu = 0.01; % fluid viscosity (cgs)
N = 50; % number of terms to approximate infinite sum in analytical solution

f_sheath_offset = -offset_circles_analytical(r_sc,r_sh,phi,N)/(L_sheath*mu); % upper bound on flow rate
f_sheath_upper_bound = -offset_circles_analytical(r_sc,r_sh_max,phi,N)/(L_sheath*mu); % take into account manufacturing tolerance
f_sheath_concentric =  pi/(8*mu*L_sheath)*(r_sh^4-r_sc^4-(r_sh^2-r_sc^2)^2/(log(r_sh/r_sc))); % lower bound on flow rate
f_sheath_lower_bound = pi/(8*mu*L_sheath)*(r_sh_min^4-r_sc^4-(r_sh_min^2-r_sc^2)^2/(log(r_sh_min/r_sc))); % take into account manufacturing tolerance
R_scope = 1/(pi*r_wc^4/(8*mu*L_scope));

%% model prediction

lin_inlet_pressure = linspace(0,5.5*10^5,50);
p0_offset=lin_inlet_pressure/(1+f_sheath_offset*R_scope);
p0_concentric=lin_inlet_pressure/(1+f_sheath_concentric*R_scope);
flow_offset = f_sheath_offset*p0_offset;
flow_concentric = f_sheath_concentric*p0_concentric;
f_optimal = 5.635*f_sheath_concentric;

% Take into account manufacturing tolerance:

p0_upper=lin_inlet_pressure/(1+f_sheath_offset*R_scope);
p0_lower=lin_inlet_pressure/(1+f_sheath_concentric*R_scope);
p0_optimal=lin_inlet_pressure/(1+f_optimal*R_scope);
flow_upper = f_sheath_offset*p0_upper;
flow_lower = f_sheath_concentric*p0_lower;

x = [lin_inlet_pressure/(rho*g)];                  
y1 = [p0_lower/(rho*g)];                     
y2 = [p0_upper/(rho*g)];                  
X=[x,fliplr(x)];                
Y=[y1,fliplr(y2)];            
pred = fill(X,Y, [0.9 0.9 0.9]);  
set(pred,'LineWidth',3)
hold on
num_kidneys=3;

for i = 1:num_kidneys
    
    if i == 1
        num_data_sets=5; 
    elseif i == 2
        num_data_sets = 2;
    elseif i == 3
        num_data_sets = 4;
    end
    for k = 1:num_data_sets
        dataset=sprintf('ss-data/K%.0f_%.0f_ss',i,k);
        load(dataset);
        for j = 1:length(mean_inlet_pressure)
            plt(:,i)=errorbarxy(mean_inlet_pressure(j)/(rho*g),mean_kidney_pressure(j)/(rho*g),(sqrt(std_inlet_pressure(j)^2+systematic_pressure_meas_error^2))/(rho*g),(sqrt(std_kidney_pressure(j)^2+systematic_pressure_meas_error^2)/(rho*g)),'Color',colors(i,:),'Linestyle','none','Marker','o','MarkerFaceColor','w','Linewidth',2,'MarkerSize',8);
            hold on
        end
        %% write data to csv: (pressure in cmH2O)
        
        headers = {'in_p','pk','in_psd','pksd'};
        data = [mean_inlet_pressure/(rho*g) mean_kidney_pressure(1:length(mean_inlet_pressure))'/(rho*g) sqrt(std_inlet_pressure.^2+systematic_pressure_meas_error^2)/(rho*g) sqrt(std_inlet_pressure.^2+systematic_pressure_meas_error^2)/(rho*g)];
        filename = sprintf('csv-files/fullsystemdata-pressure-k%i-%i.csv',i,k);
        csvwrite_with_headers([filename],data,headers);
    end
end

%% edit plot:

figure(1)
legend([pred plt(1,:)],{'Model (inc. manufacture tol.)','Kidney 1','Kidney 2','Kidney 3'});
set(legend,'location','northwest','fontsize',14);
grid on
xlabel('Inlet Pressure (mean of 5 sec. intervals)','fontsize',18,'Interpreter','latex');
ylabel('Kidney Presure','fontsize',18,'Interpreter','latex');

%% write model prediction to csv: (pressure in cmH2O)

headers = {'p','plb','pub','optimal'};
data = [(lin_inlet_pressure/(rho*g))' p0_lower'/(rho*g) p0_upper'/(rho*g) p0_optimal'/(rho*g)];
csvwrite_with_headers(['csv-files/fullsystemmodel-pressure.csv'],data,headers)