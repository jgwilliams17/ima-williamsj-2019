clear all;
close all;

n_eta=400;

%% parameters:

q = 1.4;
Lb = 36;
La = 79;
rho = 1;
g = 981;

bayre2cmh2o=0.00101972;

r_wc = 0.06; % working-channel radius (cm)
r_sc = 0.31667/2; % outer radius of scope

% From specifications (access sheath diagram Niraj sent through):

d_sh_in = 0.152; % inches
r_sh = d_sh_in*2.54/2;

r_wt = linspace(0.001,0.05,100);

rin_no_tool = 8/(pi*r_wc^4);

phi = 0.99; % position corresponding to offset scope in sheath
mu = 0.01; % fluid viscosity (cgs)
N = 100; % number of terms to approximate infinite sum in analytical solution
f_sheath_offset = -offset_circles_analytical(r_sc,r_sh,phi,N); % upper bound on flow rate
rout = 1/f_sheath_offset;

%% make rin:

% rin = zeros(size(r_wt));
% for i = 1:length(r_wt)
%     f_tool_offset = -offset_circles_analytical(r_wt(i),r_wc,phi,N);
%     rin(i) = 1/f_tool_offset;
% end
% save('rin.mat','rin');

load('rin.mat');

%% pk:

pin = 60; % cm h2o

q = pin2q(pin,1);

pk = @(rin,pin) pin./(1+(La*rin./(Lb*rout)));

%% plot:

plot([0 r_wt],bayre2cmh2o*[pk(rin_no_tool,pin*rho*g) pk(rin,pin*rho*g)],'Linewidth',2);
hold on
plot([0 r_wt],[q*mu*Lb*rout/(rho*g) q*mu*Lb*rout/(rho*g)*ones(size(r_wt))],'k--','Linewidth',2);
xlim([0 0.05]);

%% write model prediction to csv: (pressure in cmH2O)

headers = {'rwt','pk','pk_fixed_q'};
data = [[0 r_wt]' bayre2cmh2o*[pk(rin_no_tool,pin*rho*g) pk(rin,pin*rho*g)]' [q*mu*Lb*rout/(rho*g) q*mu*Lb*rout/(rho*g)*ones(size(r_wt))]'];
csvwrite_with_headers(['csv-files/workingtool.csv'],data,headers)