clear all;
close all;
set(groot, 'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex'); set(0,'defaulttextInterpreter','latex');
colors = get(gca,'colororder'); 
addpath('useful_functions/');

%% set these if want to re-write data to csv files for plots:

write_raw_data_to_csv = true;
write_fit_to_csv = true;

incr = 2; % save every incr raw data point

% convert from cgs units to cm h2o:
barye2cmh2o=0.00101972;

% pressure measurement error (Boston Scientific Comet wire):
systematic_pressure_meas_error = 0.005*600*1333.223900000007;
all_pressure = [];
all_pressure_error = []; % systematic pressure error is different for each one b/c involves relative pressure calculation

volfun_lin = @(m,b) b*m;

%% Plot gradual
% use the gradual increase data sets to estimate kidney compliance
num_kidney = 1;
data_set = [5];
    
%% Make time-step uniform so can plot volume/pressure

dataset=sprintf('trimmed-data/K%.0f_%.0f_Trim',num_kidney,data_set);
load(dataset);
zero_mass = kidney_mass(1);
        
%% Fit compliance
kidney_mass = kidney_mass-zero_mass;
kidney_pressure = kidney_pressure*barye2cmh2o;
kidney_pressure_error = systematic_pressure_meas_error*ones(size(kidney_pressure))'*barye2cmh2o;
hold on
        
% fit exponential model to data:
volfun_exp = @(b,m) b(1).*(exp(b(2).*m)-1);
bfit_exp = nlinfit(kidney_mass,kidney_pressure,volfun_exp,[1, 0.69]);
figure(1)
plot(kidney_mass,volfun_exp(bfit_exp,kidney_mass),'r--','Linewidth',3);
hold on
plt=errorbarxy(kidney_mass,kidney_pressure,sqrt(2)*0.005*ones(size(time)),kidney_pressure_error,'Color','k','Linestyle','none','Marker','o','MarkerFaceColor','k','Linewidth',1,'MarkerSize',4);
save('expfit_A.mat','bfit_exp');

if write_raw_data_to_csv
    % write data to csv:
    headers = {'kmass','kp','kmass_sd','kp_sd'};
    data = [kidney_mass(1:incr:end) kidney_pressure(1:incr:end) sqrt(2)*0.005*ones(size(kidney_pressure(1:incr:end))) [kidney_pressure_error(1:incr:end)]'];
    filename = sprintf('csv-files/compliance-k%i-%i.csv',num_kidney,data_set);
    csvwrite_with_headers(filename,data,headers)
end

if write_fit_to_csv
    headers = {'mass','pressure'};
    data = [kidney_mass(1:incr:end) volfun_exp(bfit_exp,kidney_mass(1:incr:end))];
    csvwrite_with_headers('csv-files/compliance-fit_A.csv',data,headers)
end

%% Make plot:

set(legend,'fontsize',14,'location','northwest');
grid on
xlabel('Mass increase (g)','fontsize',18);
ylabel('Relative pressure increase (dim.less)','fontsize',18);