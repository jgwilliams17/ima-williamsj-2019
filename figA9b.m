clear all;
close all;

addpath('useful_functions/'); % add offset_circles_analytical.m

write_to_csv = true;

%% Dimensional parameters:

rho = 1;
g = 981;
Pin = rho*g*60; % hydrostatic pressure of 60 cm 
mu = 0.01;

r_wc = 0.06; % working-channel radius (cm)
r_sc = 0.31667/2; % outer radius of scope
r_sh = 0.152*2.54/2; % inner radius of sheath

barye2cmh2o=0.00101972;

La = 79;
Lb = 36;

%% Resistances (including "optimal" one):

Rin = 8/(pi*r_wc^4);
Rout_offset = -1/offset_circles_analytical(r_sc,r_sh,0.99,50);
Rout_centre = 1/(pi/8*(r_sh^4-r_sc^4-(r_sh^2-r_sc^2)^2/(log(r_sh/r_sc)))); % lower bound on flow rate
Rout_optimal = Rout_centre/5.635;

%% Analytical solutions:

load('expfit_A.mat');
C_A = bfit_exp(2);
Pstar_A = bfit_exp(1)/barye2cmh2o;
tau = @(rin,rout) 1+Pin/Pstar_A+(rin*La)/(rout*Lb);
pk_nonlin_A = @(t,rin,rout) Pin*(exp(tau(rin,rout)*(t/(La*rin*mu/(C_A*Pstar_A))))-1)./((Pin/Pstar_A)+exp(tau(rin,rout)*t/(La*rin*mu/(C_A*Pstar_A)))*(1+(rin*La)/(rout*Lb)));

load('expfit.mat');
C = bfit_exp(2);
Pstar = bfit_exp(1)/barye2cmh2o;
tau = @(rin,rout) 1+Pin/Pstar+(rin*La)/(rout*Lb);
pk_nonlin = @(t,rin,rout) Pin*(exp(tau(rin,rout)*(t/(La*rin*mu/(C*Pstar))))-1)./((Pin/Pstar)+exp(tau(rin,rout)*t/(La*rin*mu/(C*Pstar)))*(1+(rin*La)/(rout*Lb)));

%% Compare models (effect of compliance):

time_exp = linspace(0,60,100);

figure(1)
plot(time_exp,pk_nonlin_A(time_exp,Rin,Rout_centre)/(rho*g),'r','Linewidth',2)
hold on
plot(time_exp,pk_nonlin(time_exp,Rin,Rout_centre)/(rho*g),'k','Linewidth',2)

if write_to_csv
    headers = {'time','Aexp','Cexp'};
    data = [time_exp' [pk_nonlin_A(time_exp,Rin,Rout_centre)/(rho*g)]' [pk_nonlin(time_exp,Rin,Rout_centre)/(rho*g)]'];
    csvwrite_with_headers('csv-files/comparecompliance_AC.csv',data,headers);
end