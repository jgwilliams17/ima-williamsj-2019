clear all;
close all;
addpath('../../useful_functions/');

%% load simulation data:

load('p-variation.mat'); % computed max(p)-min(p) as function of Re

%% convert re to pin:

% the cavity simulations are for centred scope, so set offset = 0.

pin = re2pin(re_var(:,1),0);


%% re-dimensionalise pressure variation:

q = pin2q(pin,0);
a = 0.06;
rho = 1;
mu = 0.01;
A = pi*a^2;
umax = 2*(q/A);

var = re_var(:,2)*rho.*umax.^2;

% convert to cmh2o (from cgs barye)

barye2cmh2o=0.00101972;

%% plot:

plot(pin,var*barye2cmh2o,'Linewidth',2);

xlim([0 450]);

%% write model prediction to csv: (pressure in cmH2O)

headers = {'pin','var'};
data = [pin, var*barye2cmh2o];
csvwrite_with_headers(['../../../Thesis/chapter-5-figures/dat/pressure-variation.csv'],data,headers)