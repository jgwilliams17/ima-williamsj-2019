clear all;
close all;

set(gcf, 'Renderer', 'Painters');
set(groot,'defaultAxesTickLabelInterpreter','latex'); set(groot, 'defaultLegendInterpreter','latex'); set(0,'defaulttextInterpreter','latex');

%%

re=1500;

name = sprintf('p_re_%i_scope_dims',re);
load([name '.mat'])

rho=1;
mu=0.01;
a=0.06;
umax=re*mu/(rho*a);
bayre2cmh2o=0.00101972;

N1=800;
N2=800;

%% -------- scale matrix to size of image - smooth contour plot -------- %% 


% in the offset data from paraview there was a NaN value - I'll just
% remove it:
y=y(~isnan(x));
p=p(~isnan(x));
x=x(~isnan(x));

y=y-min(min(x));

xi=linspace(min(min(x)),max(max(x)),N1);
yi=linspace(min(min(y)),max(max(y)),N2);

[XI,YI]=meshgrid(xi,yi);
pgrid=griddata(x,y,p,XI,YI)*umax^2*rho*bayre2cmh2o;
pgrid=pgrid-min(min(pgrid));

contourf(XI,YI,pgrid)

% imagesc(pgrid)

axis equal
hold on

axis equal

axis off
print([name '.eps'],'-depsc')
colorbar