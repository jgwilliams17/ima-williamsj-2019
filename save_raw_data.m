function [] = save_raw_data(kid_num,data_num,incr,incrm)

    addpath('useful_functions/'); % add csvwrite

    in_filename = sprintf('raw-data/K%i_%i',kid_num,data_num);
    load(in_filename);
    
    %% write data to csv:
    
    headers = {'time','inlet_p','pk'};
    headers_mass = {'time','mk','outm'};
    % data files are very large - make more sparse with incrementing - inc
    % = 200 looks good.
    data = [time_pressure_data([1:incr:length(time_pressure_data)]) inlet_pressure([1:incr:length(time_pressure_data)]) kidney_pressure([1:incr:length(time_pressure_data)])];
    data_mass = [time_mass_data([1:incrm:length(time_mass_data)]) kidney_mass([1:incrm:length(time_mass_data)]) out_mass([1:incrm:length(time_mass_data)])];
    out_filename = sprintf('csv-files/raw-data-k%i_%i.csv',kid_num,data_num);
    out_filename_mass = sprintf('csv-files/raw-data-mass-k%i_%i.csv',kid_num,data_num);
    csvwrite_with_headers(out_filename,data,headers)
    csvwrite_with_headers(out_filename_mass,data_mass,headers_mass)
    
end