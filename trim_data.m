function [time, kidney_pressure, inlet_pressure, out_mass, kidney_mass] = trim_data(num_kidney)

% trim the data to be just between where switch on flow and switch off.
% this is used for estimating kidney compliance from the data.

    %% load data set
    
    if num_kidney == 1
        num_data_sets = 5;
        min_t = [16 13 15 12 4]; % time (in s) switch on flow for each data set
        max_t = [252 365 352 326 110]; % time (in s) stop pressure increase
    elseif num_kidney == 2
        num_data_sets = 2;
        min_t = [12 43];
        max_t = [320 382];
    elseif num_kidney == 3
        num_data_sets = 4;
        min_t = [32 15 30 13];
        max_t = [155 120 97 230];
    end
    
    for i = 1:num_data_sets
    
        %% Make time-step uniform

        dataset=sprintf('raw-data/K%.0f_%.0f',num_kidney,i);
        load(dataset);
        zero_flow_pressure = kidney_pressure(1);

        % Mass data has larger time-step than pressure data - interpolate pressure
        % data at mass time

        % Make pressures in cgs units (rather than cm H2O)

        rho = 1;
        g = 981;

        kidney_pressure = interp1(time_pressure_data,kidney_pressure,time_mass_data)*(rho*g);
        inlet_pressure = interp1(time_pressure_data,inlet_pressure,time_mass_data)*(rho*g);
        time = time_mass_data;

        %% Look at section between min_t & max_t

        indx = find(time > min_t(i) & time < max_t(i));

        kidney_pressure = kidney_pressure(indx);
        inlet_pressure = inlet_pressure(indx);
        time = time(indx);
        out_mass = out_mass(indx);
        kidney_mass = kidney_mass(indx);

        filename = sprintf('trimmed-data/K%.0f_%.0f_Trim.mat',num_kidney,i);
        save(filename,'kidney_pressure','inlet_pressure','time','out_mass','kidney_mass');
    
    end
    
end
