function [time, kidney_pressure, inlet_pressure, out_mass, kidney_mass] = trim_data_load_unload(num_kidney)

% trim the data to be just between where switch on flow and switch off.
% this is used for estimating kidney compliance from the data.

    %% load data set
    
    if num_kidney == 1
        num_data_sets = 1;
        data_sets = [5];
        min_t = [4]; % time (in s) switch on flow for each data set
        max_t = [199];
    elseif num_kidney == 2
        num_data_sets = 0;
        data_sets = [];
    elseif num_kidney == 3
        num_data_sets = 3;
        data_sets = [1 2 3];
        min_t = [32 15 30];
        max_t = [216 158 133];
    end
    
    for i = 1:num_data_sets
        
        %% Make time-step uniform
        
        dataset=sprintf('raw-data/K%.0f_%.0f',num_kidney,data_sets(i));
        load(dataset);
        zero_flow_pressure = kidney_pressure(1);

        % Mass data has larger time-step than pressure data - interpolate pressure
        % data at mass time

        % Make pressures in cgs units (rather than cm H2O)

        rho = 1;
        g = 981;

        kidney_pressure = interp1(time_pressure_data,kidney_pressure,time_mass_data)*(rho*g);
        inlet_pressure = interp1(time_pressure_data,inlet_pressure,time_mass_data)*(rho*g);
        time = time_mass_data;

        %% Look at section between min_t & max_t

        indx = find(time > min_t(i) & time < max_t(i));

        kidney_pressure = kidney_pressure(indx);
        inlet_pressure = inlet_pressure(indx);
        time = time(indx);
        out_mass = out_mass(indx);
        kidney_mass = kidney_mass(indx);

        filename = sprintf('trimmed-data/K%.0f_%.0f_Trim_Load_Unload.mat',num_kidney,data_sets(i));
        save(filename,'kidney_pressure','inlet_pressure','time','out_mass','kidney_mass');
    
    end
    
end
