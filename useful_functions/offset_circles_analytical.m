function [Q] = offset_circles_analytical(r_1,r_2,phi,N)

%% Parameters:
if r_1/r_2 < 0.05
    n_xi = 5000;
    n_eta = 5000;
else 
    n_xi = 2000;
    n_eta = 400;
end

%% Defining the domain:

gamma = r_1./r_2;
eta_1 = acosh((1./gamma).*(gamma.*(1 + phi.^2) + (1 - phi^2))./(2*phi)); % larger (smaller circle)
eta_2 = acosh((gamma*(1 - phi^2) + (1 + phi^2))./(2*phi)); % smaller (larger circle)

c = r_1.*sinh(eta_1); % constant

%% Solution:
    
n = linspace(1,N,N);

eta = linspace(eta_2,eta_1,n_eta)';
xi = linspace(0,2*pi,n_xi)';

x = zeros(length(eta)*length(xi),1);
y = zeros(length(eta)*length(xi),1);

v = zeros(length(xi),length(eta));

F = (eta_1*coth(eta_2) - eta_2*coth(eta_1))/(2*(eta_1-eta_2));
E = (coth(eta_1) - coth(eta_2))/(2*(eta_1-eta_2));

A_n = (coth(eta_1) - coth(eta_2))./(exp(2*n*eta_1)-exp(2*n*eta_2));
B_n = (exp(2*n*eta_1)*coth(eta_2)-exp(2*n*eta_2)*coth(eta_1))./(exp(2*n*eta_1)-exp(2*n*eta_2));

count = 1;
for i = 1:length(xi)
    for j = 1:length(eta)
        sum = 0;
        for k = 1:N
            sum = (A_n(k)*exp(k*eta(j)) + (B_n(k)-coth(eta(j))).*exp(-k*eta(j))).*cos(k*xi(i)) + sum;
        end
        v(i,j) = F + E*eta(j) - coth(eta(j))/2 + sum;
        x(count) = c.*sinh(eta(j))./(cosh(eta(j))-cos(xi(i)));
        y(count) = c.*sin(xi(i))./(cosh(eta(j))-cos(xi(i)));
        count = count+1;
    end
end

u = -v*c^2;

integrad = zeros(size(u));

for i = 1:n_eta
    integrad(:,i) = u(:,i)./((cosh(eta(i)) - cos(xi)).^2);
end

Q = c^2*trapz(eta,trapz(xi,integrad));
% plot(x,y,'x');
axis equal
end